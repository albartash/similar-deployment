from smart_env import ENV

ENV.enable_automatic_type_cast()

from similar_app.db.base import database
from similar_app.db.text import Text

database.create_tables([Text])
