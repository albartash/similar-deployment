#!/bin/bash

cd backend

echo "DATABASE: "$DATABASE

exec gunicorn --workers=2 -b 0.0.0.0:5000 app:app
