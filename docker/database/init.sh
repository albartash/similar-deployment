#!/bin/bash

USERNAME=similar
PASSWD=similar
DBNAME=similar_db

psql postgres -tAc "SELECT 1 FROM pg_roles WHERE rolname='$USERNAME'" | grep -q 1 \
     || createuser -U postgres $USERNAME && psql -U postgres -c "ALTER USER $USERNAME WITH PASSWORD $PASSWD" \
     || exit 1


if psql -lqt | cut -d \| -f 1 | grep -qw $DBNAME ; then
    echo "DATABASE EXISTS"
else
    createdb -u postgres $DBNAME && \
    psql -u postgres -c "GRANT ALL PRIVILEGES ON DATABASE $DBNAME TO $USERNAME" || \
    exit 2
fi

